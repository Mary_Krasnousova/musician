﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Musician
{
    abstract class AbstractMusic
    {
        private string _name;
        private double _duration;
        private double _songVolume;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public double Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }
        public double SongVolume
        {
            get { return _songVolume; }
            set { _songVolume = value; }
        }
    }
}
