﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Musician
{
    class Assembly
    {
        private List<AbstractMusic> _music;

        public void AddMusic(AbstractMusic composition)
        {
            _music.Add(composition);
        }

        public void DeleteMusic(AbstractMusic composition)
        {
            _music.Remove(composition);
        }

    }
}
